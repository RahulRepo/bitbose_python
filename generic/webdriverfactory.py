from selenium import webdriver


class WebDriverFactory():

    def __init__(self, browser):
        self.browser = browser

    def getWebDriverInstance(self):
        baseURL = "https://dev.bitbose.com/"
        if self.browser == "ie":
            driver = webdriver.Ie(executable_path=
                                  "C:\\Users\\RevInfotech\\PycharmProjects\\eprofitanalyze\\exefiles\\IEDriverServer.exe")
        elif self.browser == "chrome":
            driver = webdriver.Chrome(executable_path=
                                      "C:\\Users\\RevInfotech\\PycharmProjects\\eprofitanalyze\\exefiles\\chromedriver.exe")
        elif self.browser == "firefox":
            driver = webdriver.Firefox(executable_path=
                                       "C:\\Users\\RevInfotech\\PycharmProjects\\eprofitanalyze\\exefiles\\geckodriver.exe")
        else:
            driver = webdriver.Firefox()
        driver.implicitly_wait(3)
        driver.maximize_window()
        driver.get(baseURL)
        return driver
