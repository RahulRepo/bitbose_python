from selenium import webdriver
from pages.BitBose_Locators import locator
from generic.Selenium_driver import SeleniumDriver
import time
from generic.basepage import BasePage
import utilities.customlogger as cl
import logging


class signuppage(BasePage):
    log = cl.customLogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    def LoginBtn(self):
        self.elementClick(locator.Loginbtn, locatorType="xpath")

    def SignUPLoginBtn(self):
        self.elementClick(locator.SignUpBtn, locatorType="xpath")

    def SignValid(self):
        self.LoginBtn()
        self.SignUPLoginBtn()
        # self.sendKeys(Name, locator.FullName)
        # self.sendKeys(mail, locator.Email)
        # self.sendKeys(phone, locator.PhoneNo)
        # self.sendKeys(password, locator.Password)
        # self.sendKeys(Confirmpassword, locator.ConfPassword)
        # self.elementClick(locator.ContinueBtn, loatorType="xpath")
