import pytest
from generic.webdriverfactory import WebDriverFactory

# URL_Under_Test

baseURL = "https://dev.bitbose.com/"


@pytest.yield_fixture()
def setUp():
    print("Running method level setUp")
    yield
    print("Running method level tearDown")


@pytest.yield_fixture(scope="class")
def oneTimeSetUp(request, browser):
    print("Running one time setUp")
    wdf = WebDriverFactory(browser)
    driver = wdf.getWebDriverInstance()

    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()
    print("Running one time tearDown")


def pytest_addoption(parser):
    parser.addoption("--browser")
    parser.addoption("--osType", help="Type of operating system")


@pytest.yield_fixture(scope="session")
def browser(request):
    return request.config.getoption("--browser")


@pytest.yield_fixture(scope="session")
def osType(request):
    return request.config.getoption("--osType")
