from pages.signup import signuppage
import unittest
import pytest
from utilities.teststatus import TestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class SignUpTests(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self):
        self.sp = signuppage(self.driver)
        self.ts = TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_SignUpValid(self):
        self.sp.SignValid()
