#!C:\Users\RevInfotech\PycharmProjects\eprofitanalyze\venv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'base58==0.2.1','console_scripts','base58'
__requires__ = 'base58==0.2.1'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('base58==0.2.1', 'console_scripts', 'base58')()
    )
